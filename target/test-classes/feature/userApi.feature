Feature: Post method test
  Background:
    Given http baseUri is "https://petstore.swagger.io/v2"

  Scenario Outline: Add user to PetStore
    When I set body to post user with details
      | id | username | firstname | lastname | email   | password | phone  | userStatus |
      | <id> | <username> | <firstname> | <lastname> | <email> | <password> | <phone> | <userStatus> |
    And I POST "/user"
    And http response code should be 200
    And http response header Content-Type should be "application/json"
    Examples:
      | id | username | firstname | lastname | email             | password | phone       | userStatus |
      | 101  | vb073  | ram     | patil      | johndoe@example.com | johnDoe  | 1234567890  | 1          |
      | 2  |  vb08  | Jane      |  patel      | janedoe@example.com | pass456  | 9876543210  | 1          |
      |  4   | vb09      |     modi    |  jii        |   politica@tmc  |9876543210|9876543210|    1      |

 Scenario Outline:  login the user using username and password
    When the user is login "<username>" and "<password>"
    Then verify the user is logedin succesful
   Examples:
     | username | password |
     | vishal   | vishal@123 |
     | rishabh  | broo@123   |


   Scenario Outline:  get user by username
     When  i am searching user by user name "/user/" and enter "<names>"
     Then user will be visible
     Examples:
      |names|
      |vb073|
      |vb08 |


     


