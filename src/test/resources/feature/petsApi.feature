
Feature: Pet Store API - Add Pet

  Background:
    Given baseUri is :  "https://petstore.swagger.io/v2/pet/"

  Scenario Outline: Add a new pet
    And the pet information:
      | id     | category_id | category_name | name   | photo_urls | tag_id | tag_name | status     |
      | <id>   | <cat_id>     | <cat_name>    | <name> | <photo>    | <tag_id>| <tag_name>| <status>   |
    When I add the pet
    Then the pet is added successfully
    And I validate with status code 200

    Examples:
      | id | cat_id | cat_name | name   | photo        | tag_id | tag_name | status     |
      | 1  | 1      | Cat      | CatDog | dog.jpg      | 1      | Dog      | available  |
      | 2  | 2      | Dog      | DogCat | cat.jpg      | 2      | Cat      | pending    |
