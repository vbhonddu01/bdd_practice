package testRunner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber .class)
@CucumberOptions(
        features = "/home/vishal/IdeaProjects/bdd_practice/src/test/resources/feature/petsApi.feature",
        glue = "stepDefination",
        dryRun = false,
        monochrome = true,
        plugin = {"pretty", "html:test-output"}

)
public class petsRunner {
}
