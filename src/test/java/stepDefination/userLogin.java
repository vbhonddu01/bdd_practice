package stepDefination;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import pages.user;

public class userLogin {
    private Response response;
    private RequestSpecification request;
    String baserurl;
     user users = new user();


    @When("the user is login {string} and {string}")
    public void the_user_is_login_and(String id, String pass) {
      response = users.login(id, pass);
    }

    @Then("verify the user is logedin succesful")
    public void verify_the_user_is_logedin_succesful() {
        Assert.assertEquals(200, response.getStatusCode());
    }


}
