package stepDefination;

import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import pages.user;

public class getUser {
    String baserurl = "https://petstore.swagger.io/v2";
    Response response;

    user users = new user();

    @When("i am searching user by user name {string} and enter {string}")
    public void i_am_searching_user_by_user_name(String ep , String name) {
       response = users.getUser(baserurl + ep , name);
    }

    @Then("user will be visible")
    public void user_will_be_visible() {
        if (response.getStatusCode() != 200){
            System.out.println("user Not Exist");
        }
    }




}
