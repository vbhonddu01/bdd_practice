package stepDefination;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import pages.loginPage;

public class steps {
    public WebDriver driver;
    public loginPage signin;

    @Given("user Launch Chrome Browser")
    public void user_launch(){
        System.setProperty("webdriver.chrome.driver" , "/home/vishal/Downloads/chromedriver.exe");
        driver = new ChromeDriver();
        signin = new loginPage(driver);

    }
    @When("user opens {string}")
    public  void open_url(String url){
        driver.get(url);

    }
    @When("user Enters Email {string} and Password as {string}")
    public void entreEmailAndPass(String email , String pass){
        signin.setEmailIdInput(email);
        signin.setPassword(pass);

    }
    @When("Click on login")
    public void clickButton(){
        signin.setLoginButton();
    }
    @Then("page title should be {string}")
    public void pageTitle(String title){
        if (driver.getPageSource().contains("Login was unsuccessful.")){
            driver.close();
        }else{
            Assert.assertEquals(title , driver.getTitle());
        }
    }
    @Then("close browser")
    public  void closeBrowser(){
        driver.quit();
    }
}
