package stepDefination;

import io.cucumber.java.en.*;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import pages.user;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static payload.userPaylod.createPayload;

public class usrStepDefination {

    private Response response;
    private RequestSpecification request;
    String baserurl;
    user users = new user();
    Map<String, Object> payload = new HashMap<>();
    @Given("http baseUri is {string}")
    public void http_base_uri_is(String endpoint) {
        baserurl = endpoint;
    }

    @When("I set body to post user with details")
    public void i_set_body_to_post_user_with_details(List<Map<String , String>>data) {
        Map<String ,String>userData = data.get(0);
        payload = createPayload(userData.get("id"), userData.get("username"), userData.get("firstname"),
                userData.get("lastname"), userData.get("email"), userData.get("password"),
                userData.get("phone"), userData.get("userStatus"));
    }

    @And("I POST {string}")
    public void i_post(String endpt) {
        response = users.postUser(baserurl + endpt , payload);
    }

    @And("http response code should be {int}")
    public void http_response_code_should_be(int statusCode) {
        int res = response.getStatusCode();
        Assert.assertEquals(statusCode , res);
    }

    @And("http response header Content-Type should be {string}")
    public void http_response_header_content_type_should_be(String string) {
       String type = response.getContentType();
       Assert.assertEquals(string , type);
    }

}
