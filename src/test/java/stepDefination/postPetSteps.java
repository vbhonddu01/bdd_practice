package stepDefination;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.*;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;
import pages.pets;
import pages.user;
import payload.petsPayload;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class postPetSteps {

    private Response response;
    private RequestSpecification request;
    String baserurl;

    petsPayload pet = new petsPayload();
    private List<Map<String, String>> petDataRows;

    pets pp = new pets();

    @Given("baseUri is :  {string}")
    public void base_uri_is(String url) {
        baserurl = url;

    }

    @And("the pet information:")
    public void the_pet_information(DataTable dataTable) {
        petDataRows = dataTable.asMaps(String.class, String.class);
        for (Map<String, String> row : petDataRows) {
            int id = getIntValue(row, "id");
            String name = getStringValue(row, "name");
            String status = getStringValue(row, "status");
            int catId = getIntValue(row, "catId");
            String catName = getStringValue(row, "catName");
            int tagId = getIntValue(row, "tagId");
            String tagName = getStringValue(row, "tagName");
            String photoUrl = getStringValue(row, "photoUrl");
            pet.getPetDataAsListOrMap(id, catId, catName, tagId, tagName, name, photoUrl, status);
        }
    }

    private int getIntValue(Map<String, String> row, String key) {
        String value = row.get(key);
        return value != null && !value.isEmpty() ? Integer.parseInt(value) : 0;
    }

    private String getStringValue(Map<String, String> row, String key) {
        return row.getOrDefault(key, "");
    }


    @When("I add the pet")
    public void i_add_the_pet() {
        response = pets.postPet(baserurl  , petDataRows );

    }

    @Then("the pet is added successfully")
    public void the_pet_is_added_successfully() {
        System.out.println(response);
    }

    @And("I validate with status code {int}")
    public void i_validate_with_status_code(int statusCode) {

        if (response.statusCode() > statusCode){
            System.out.println("please check data Table ");
        }
//        try {
//            Assert.assertEquals(statusCode, response.getStatusCode());
//        } catch (AssertionError e) {
//            // Handle the assertion error, such as logging or rethrowing
//            System.out.println("Assertion error occurred: " + e.getMessage());
//            throw e; // Rethrow the error to fail the step
//        }

    }
}
