package payload;

import java.util.*;

public class petsPayload {

    public static List<Map<String, Object>> getPetDataAsListOrMap(int id , int catid , String catName , int tagId , String tagname , String name , String photoUrl , String status) {
        List<Map<String, Object>> petDataList = new ArrayList<>();
        Map<String, Object> petData = new HashMap<>();

        // Create category object
        category cat = new category();
        cat.setId(catid);
        cat.setName(catName);

        // Create tag object
        tag tags = new tag();
        tags.setId(tagId);
        tags.setName(tagname);

        // Set values for petData map
        petData.put("id", id);
        petData.put("category", cat);
        petData.put("name", name);
        petData.put("photoUrls", List.of(photoUrl));
        petData.put("tags", List.of(tags));
        petData.put("status", status);

        petDataList.add(petData);

        return petDataList;
    }
}
