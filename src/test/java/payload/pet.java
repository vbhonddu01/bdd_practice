package payload;

public class pet {

    private long id;
    private category category;
    private String name;
    private String[] photoUrls;
    private  tag[] tags;
    private String status;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public payload.category getCategory() {
        return category;
    }

    public void setCategory(payload.category category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getPhotoUrls() {
        return photoUrls;
    }

    public void setPhotoUrls(String[] photoUrls) {
        this.photoUrls = photoUrls;
    }

    public tag[] getTags() {
        return tags;
    }

    public void setTags(tag[] tags) {
        this.tags = tags;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
