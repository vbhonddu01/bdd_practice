package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class loginPage {
    public WebDriver driver;

    @FindBy(id = "Email")
    @CacheLookup
    private WebElement emailIdInput;

    @FindBy(id = "Password")
    @CacheLookup
    private WebElement password;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement loginButton;

    public  loginPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver , this);
    }

   public void setEmailIdInput(String mail){
        emailIdInput.clear();
        emailIdInput.sendKeys(mail);
   }
   public void setPassword(String pass){
        password.clear();
        password.sendKeys(pass);
   }
   public  void setLoginButton(){
        loginButton.click();
   }
}
