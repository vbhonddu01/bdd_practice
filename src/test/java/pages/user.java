package pages;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.util.Map;

import static io.restassured.RestAssured.given;

public class user {
    public Response postUser(String endpoint , Map<String , Object>payload){
        Response response = given()
                .contentType(ContentType.JSON)
                .body(payload)
                .when()
                .post(endpoint);
        return  response;
    }

    public  Response login(String id , String pass ){
        RestAssured.baseURI = "https://petstore.swagger.io/v2/user/login";
        Response response = given()
                .queryParam("username" , id)
                .queryParam("password" , pass)
                .header("accept" ,"application/json")
                .when()
                .get();
        return response;
    }

    public Response getUser(String endPoint,String username){
        Response response = given()
                .body(username)
                .when()
                .get(endPoint);

        return  response;
    }
}


//https://petstore.swagger.io/v2/pet/