package pages;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class pets {

    public static Response postPet(String endpoint, List<Map<String, String>> payload){
        Response response = given()
                .contentType(ContentType.JSON)
                .body(payload)
                .when()
                .post(endpoint);

        return  response;

    }
}
